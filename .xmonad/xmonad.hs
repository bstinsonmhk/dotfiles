import XMonad
import XMonad.Config.Gnome
import XMonad.Layout.NoBorders
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.SetWMName
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.UrgencyHook
import XMonad.Hooks.FadeInactive
import XMonad.Hooks.EwmhDesktops
import XMonad.Util.EZConfig

import XMonad.Prompt
import XMonad.Prompt.RunOrRaise

import XMonad.Util.Run
import qualified Data.Map as M

myWorkspaces = ["web","comm-tmux","vim","srv-tmux","logs"] ++ map show[6..9 :: Int]

myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
  [ ((modm .|. controlMask, xK_l),spawn "DISPLAY=:0.0 /usr/bin/gnome-screensaver --lock")
  , ((modm .|. shiftMask, xK_p), runOrRaisePrompt myXPConfig)
  ]
  
myLogHook h = dynamicLogWithPP $ defaultPP
    {
        ppCurrent           =   dzenColor "#ebac54" "#1B1D1E" . pad
      , ppVisible           =   dzenColor "white" "#1B1D1E" . pad
      , ppHidden            =   dzenColor "white" "#1B1D1E" . pad
      , ppHiddenNoWindows   =   dzenColor "#7b7b7b" "#1B1D1E" . pad
      , ppUrgent            =   dzenColor "#ff0000" "#1B1D1E" . pad
      , ppWsSep             =   " "
      , ppSep               =   "  |  "
      , ppTitle             =   (" " ++) . dzenColor "white" "#1B1D1E" . dzenEscape
      , ppOutput            =   hPutStrLn h
    }

myStartupHook = do
      spawn "xset -b"
      spawn "xrdb -merge ${HOME}/.Xresources"
      
myXPConfig :: XPConfig
myXPConfig = defaultXPConfig {
              height = 20
            }

myXmonadBar = "dzen2 -x '1680' -y '0' -h '24' -w '640' -ta 'l' -fg '#FFFFFF' -bg '#1B1D1E'"
main = do
    dzenLeftBar <- spawnPipe myXmonadBar
    xmonad $ gnomeConfig {
              terminal = "gnome-terminal",
              layoutHook = smartBorders(layoutHook gnomeConfig),
              logHook = myLogHook dzenLeftBar,
              startupHook = myStartupHook,
              keys = myKeys <+> keys defaultConfig,
              workspaces = myWorkspaces
            }
