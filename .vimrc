let mapleader=","             " change the leader to be a comma vs slash
let maplocalleader=","             " change the leader to be a comma vs slash

call plug#begin()

"======================================
" My vundle Plugins
"======================================
" Fuzzy file Finder
Plug 'kien/ctrlp.vim'

" Directory Tree Explorer
Plug 'scrooloose/nerdtree'
"Plug 'Xuyuanp/nerdtree-git-plugin'

" Git Integration
Plug 'tpope/vim-fugitive'

" Work with brackets, quotes, and others
Plug 'tpope/vim-surround'

" Snippets
Plug 'MarcWeber/vim-addon-mw-utils'
Plug 'tomtom/tlib_vim'
Plug 'garbas/vim-snipmate'
Plug 'honza/vim-snippets'

" Syntax checker, python in particular
Plug 'scrooloose/syntastic'

" Highlighting for Asciidoc
Plug 'dagwieers/asciidoc-vim'

" More python highlighting
Plug 'hdima/python-syntax'

" Markdown Highlighting
Plug 'tpope/vim-markdown'

" Buffer/Tab tagbar (at the top)
Plug 'majutsushi/tagbar'

" Fork of Powerline (statusbar at the bottom)
Plug 'bling/vim-airline'

" Powerline Themes
Plug 'vim-airline/vim-airline-themes'

" Required for tagbar
Plug 'corntrace/bufexplorer'

" Colorscheme
Plug 'nanotech/jellybeans.vim'

" Pytest TDD Plugin
Plug 'alfredodeza/pytest.vim'

" jedi-vim
Plug 'davidhalter/jedi-vim'

" vimwiki
Plug 'vimwiki/vimwiki', {'branch': 'dev'}

" vimwiki/taskwarrior integration
"Plug 'DancingQuanta/taskwiki', {'branch': 'extra-syntaxes'}

" a calendar
Plug 'itchyny/calendar.vim'

" git gutter
Plug 'airblade/vim-gitgutter'

" go support
Plug 'fatih/vim-go'

" more completion/nav
" Plugin 'vim-ctrlspace/vim-ctrlspace'        " Tabs/Buffers/Fuzzy/Workspaces/Bookmarks
Plug 'mileszs/ack.vim'                    " Ag/Grep
Plug 'fisadev/FixedTaskList.vim'          " Pending tasks list
Plug 'MattesGroeger/vim-bookmarks'        " Bookmarks
Plug 'thaerkh/vim-indentguides'           " Visual representation of indents
Plug 'neomake/neomake'                    " Asynchronous Linting and Make Framework
Plug 'Shougo/deoplete.nvim'               " Asynchronous Completion
Plug 'roxma/nvim-yarp'                    " Deoplete Dependency #1
Plug 'roxma/vim-hug-neovim-rpc'           " Deoplete Dependency #2
Plug 'ryanoasis/vim-devicons'
Plug 'scrooloose/nerdcommenter' 

Plug 'klen/python-mode', {'branch': 'develop'}                   " Python mode (docs, refactor, lints...)
Plug 'hynek/vim-python-pep8-indent'
Plug 'mitsuhiko/vim-python-combined'
Plug 'mitsuhiko/vim-jinja'
Plug 'jmcantrell/vim-virtualenv'

call plug#end()

"===============================================================
" Basic Settings
" ==============================================================
set nocompatible        " use vim defaults
set history=256		    " save 256 lines of command line history
set cf 				    " error files
set ls=2                " allways show status line
set scrolloff=3         " keep 3 lines when scrolling
set tw=80               " wrap at 80 columns
set modeline            " last lines in document sets vim mode
set whichwrap=b,s,h,l,<,>,[,]   " move freely between files
set isk+=_,$,@,%,#,-    "more word dividers
set autoread		    " reload files
set ruler			    " shows us the cursor position
set number              " show numbers (relative numbers don't work on vim 7.2)
set noautowrite		    " write on :make or :! commands
set noautowriteall
set noautoread
set t_Co=256            " use 256 colors, most terms support this
set title               " show title in the console bar
set wildmenu            " Menu completion in command mode on <Tab>
set wildmode=longest,list " <Tab> cycles between all matching choices
set wildignore+=*.o,*.obj,.git,*.pyc " Ignore these files when completing
set confirm
set showcmd
"set list                " show spaces and tabs
set formatoptions -=tc
set encoding=utf-8

filetype plugin indent on

"===============================================================
" Color/Theme Settings
"===============================================================
syntax on
let base16colorspace=256
set background=dark     " adapt colors for background
colorscheme jellybeans "use this color scheme [BE SURE THE SCHEME IS DISTRIBUTED!]
highlight clear Search
highlight Search term=reverse cterm=bold ctermbg=1 guifg=white guibg=hotpink1
set laststatus=2


"===============================================================
" Deoplete Settings
"===============================================================
let g:deoplete#enable_at_startup = 1

"===============================================================
" Searches and visuals
"===============================================================
set showmatch			" show brace matches
set incsearch       	" do incremental searching
set ignorecase          " ignore case when searching
set visualbell			" I don't care if you want my attention

"===============================================================
" TMP and Backups
"===============================================================
set backup
set backupdir=~/tmp
set directory=~/tmp

"===============================================================
"Formatting/Moving/Editing
"===============================================================
set autoindent		    " fix weird autoindent indent
"set nosmartindent		" don't try to be too clever
set nowrap linebreak    " wrap lines
set expandtab			" convert tabs to spaces
"set tabstop=4			" Python would murder me without a plugin
set shiftwidth=4        " Set indent shiftwidth
set softtabstop=4		" softtabs are 4 spaces
set textwidth=79		" set the default textwidth to 79 chars
set virtualedit=block	" visual edit is a block as it appears on the screen
set foldmethod=marker   " let us fold on indents
set foldlevel=99        " don't fold by default
set backspace=2         " Allow backspace over autoindent, EOL, and BOL
set shiftround
set listchars=tab:›\ ,trail:•,extends:#,nbsp:. " displays tabs with :set list & displays when a line runs off-screen
set list

"===============================================================
"Keymaps
"===============================================================

let g:python_host_prog = '/usr/bin/python'

" ctrl-jklm  changes to that split
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h


" and lets make these all work in insert mode too ( <C-O> makes next cmd
"  happen as if in command mode )
imap <C-W> <C-O><C-W>

map <F7> :set spell! spell? spl=en_us<cr>

if has("gui_running")
    set columns=205      " 80 column width in GUI mode
    set selectmode=mouse
    set keymodel=
    set guioptions-=l
    set guioptions-=r
    set guioptions-=b
    set guioptions-=T
    set guioptions-=L
    set guioptions-=m
    set gfn=Source\ Code\ Pro\ for\ Powerline\ 10
endif

if has("autocmd")
    " Restore cursor position
    au BufReadPost * if line("'\"") > 0|if line("'\"") <= line("$")|exe("norm '\"")|else|exe "norm $"|endif|endif
    au FileType text setlocal spell spelllang=en_us
    au FileType gitcommit setlocal spell textwidth=72
    au FileType mail setlocal spell spelllang=en_us
    au FileType mail setlocal tw=72
    au FileType yaml setlocal ts=2 sts=2 sw=2 expandtab indentkeys-=0# indentkeys-=<:>
endif

function! Startup()
    if 0 == argc()
        NERDTree
        wincmd l        " on startup, make sure we end up in the middle pane
    end
endfunction
au VimEnter * call Startup()


"===============================================================
" Python
"===============================================================
" Python Options
"au FileType python set sw=4 sts=4 et
"au BufRead *.py set efm=%C\ %.%#,%A\ \ File\ \"%f\"\\,\ line\ %l%.%#,%Z%[%^\ ]%\\@=%m

" python executables for different plugins
let g:pymode_python='python'

" rope
let g:pymode_rope=0
let g:pymode_rope_completion=0
let g:pymode_rope_complete_on_dot=0
let g:pymode_rope_auto_project=0
let g:pymode_rope_enable_autoimport=0
let g:pymode_rope_autoimport_generate=0
let g:pymode_rope_guess_project=0

" documentation
let g:pymode_doc=1
let g:pymode_doc_bind='K'

" lints
let g:pymode_lint=1
let g:pymode_lint_ignore=['E501']

" virtualenv
let g:pymode_virtualenv=1

" breakpoints
let g:pymode_breakpoint=1
let g:pymode_breakpoint_key='<leader>b'

" syntax highlight
let g:pymode_syntax=1
let g:pymode_syntax_slow_sync=1
let g:pymode_syntax_all=1
let g:pymode_syntax_print_as_function=g:pymode_syntax_all
let g:pymode_syntax_highlight_async_await=g:pymode_syntax_all
let g:pymode_syntax_highlight_equal_operator=g:pymode_syntax_all
let g:pymode_syntax_highlight_stars_operator=g:pymode_syntax_all
let g:pymode_syntax_highlight_self=g:pymode_syntax_all
let g:pymode_syntax_indent_errors=g:pymode_syntax_all
let g:pymode_syntax_string_formatting=g:pymode_syntax_all
let g:pymode_syntax_space_errors=g:pymode_syntax_all
let g:pymode_syntax_string_format=g:pymode_syntax_all
let g:pymode_syntax_string_templates=g:pymode_syntax_all
let g:pymode_syntax_doctests=g:pymode_syntax_all
let g:pymode_syntax_builtin_objs=g:pymode_syntax_all
let g:pymode_syntax_builtin_types=g:pymode_syntax_all
let g:pymode_syntax_highlight_exceptions=g:pymode_syntax_all
let g:pymode_syntax_docstrings=g:pymode_syntax_all

" highlight 'long' lines (>= 80 symbols) in python files
augroup vimrc_autocmds
    autocmd!
    autocmd FileType python,rst,c,cpp highlight Excess ctermbg=DarkGrey guibg=Black
    autocmd FileType python,rst,c,cpp match Excess /\%81v.*/
    autocmd FileType python,rst,c,cpp set nowrap
    autocmd FileType python,rst,c,cpp set colorcolumn=80
augroup END

" code folding
let g:pymode_folding=0

" pep8 indents
let g:pymode_indent=1

" code running
let g:pymode_run=1
let g:pymode_run_bind='<F5>'

let g:ale_sign_column_always = 0
let g:ale_emit_conflict_warnings = 0                                                                         
let g:airline#extensions#ale#enabled = 1
let g:pymode_rope_lookup_project = 0
let g:airline#extensions#tabline#enabled = 1

" Add the virtualenv's site-packages to vim path
python << EOF
import os.path
import sys
import vim
if 'VIRTUALENV' in os.environ:
    project_base_dir = os.environ['VIRTUAL_ENV']
    sys.path.insert(0, project_base_dir)
    activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
    execfile(activate_this, dict(__file__=activate_this))
EOF


"===============================================================
" airline options
"===============================================================
let g:airline_theme='distinguished'
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1

"===============================================================
" tagbar options
"===============================================================
let g:tagbar_width=25
map <leader>j :TagbarOpen<return>
autocmd BufEnter *.py :call tagbar#autoopen(0)
autocmd BufWinLeave *.py :TagbarClose

"===============================================================
" CTRLP options
"===============================================================
" Ignore files we don't want to index
set wildignore+=*.so,*.swp,*.zip     " MacOSX/Linux
set wildignore+=*.o,*.obj,.git,*.rbc,*.class,.svn
let g:ctrlp_show_hidden=1

"===============================================================
" NERDTree options
"===============================================================
let g:NERDTreeWinSize = 25
map <leader>n :NERDTreeToggle<CR>
let NERDTreeIgnore=['\.pyc$', '\.pyo$', '__pycache__$']     " Ignore files in NERDTree

"===============================================================
" NERDComment options
"===============================================================
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1

" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'

" Set a language to use its alternate delimiters by default
let g:NERDAltDelims_java = 1

" Add your own custom formats or override the defaults
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }

" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1

" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1

"===============================================================
" DevIcon options
"===============================================================
" loading the plugin 
let g:webdevicons_enable = 1

" adding the flags to NERDTree 
let g:webdevicons_enable_nerdtree = 1

" adding to vim-airline's tabline
let g:webdevicons_enable_airline_tabline = 1

" adding to vim-airline's statusline
let g:webdevicons_enable_airline_statusline = 1

" turn on/off file node glyph decorations (not particularly useful)
let g:WebDevIconsUnicodeDecorateFileNodes = 1

" use double-width(1) or single-width(0) glyphs 
" only manipulates padding, has no effect on terminal or set(guifont) font
let g:WebDevIconsUnicodeGlyphDoubleWidth = 1

" whether or not to show the nerdtree brackets around flags 
let g:webdevicons_conceal_nerdtree_brackets = 0

" the amount of space to use after the glyph character (default ' ')
let g:WebDevIconsNerdTreeAfterGlyphPadding = ' '

" Force extra padding in NERDTree so that the filetype icons line up vertically
let g:WebDevIconsNerdTreeGitPluginForceVAlign = 1 

" change the default character when no match found
let g:WebDevIconsUnicodeDecorateFileNodesDefaultSymbol = 'ƛ'

" set a byte character marker (BOM) utf-8 symbol when retrieving file encoding
" disabled by default with no value
let g:WebDevIconsUnicodeByteOrderMarkerDefaultSymbol = ''

" enable folder/directory glyph flag (disabled by default with 0)
let g:WebDevIconsUnicodeDecorateFolderNodes = 1

" enable open and close folder/directory glyph flags (disabled by default with 0)
let g:DevIconsEnableFoldersOpenClose = 1

" enable pattern matching glyphs on folder/directory (enabled by default with 1)
let g:DevIconsEnableFolderPatternMatching = 1

" enable file extension pattern matching glyphs on folder/directory (disabled by default with 0)
let g:DevIconsEnableFolderExtensionPatternMatching = 0


"===============================================================
" vimwiki options
"===============================================================
let g:vimwiki_list = [{'path':'~/doc/wiki', 'syntax': 'markdown', 'ext':'.md'}]
let g:vimwiki_url_maxsave = 0
let g:tagbar_type_vimwiki = {
          \   'ctagstype':'vimwiki'
          \ , 'kinds':['h:header']
          \ , 'sro':'&&&'
          \ , 'kind2scope':{'h':'header'}
          \ , 'sort':0
          \ , 'ctagsbin':'~/bin/vwtags.py'
          \ , 'ctagsargs': 'default'
          \ }

let g:taskwiki_syntax = 'markdown'

"===============================================================
" jedi-vim options
"===============================================================
" don't auto type import for me after a `from <module>...` that's annoying
let g:jedi#smart_auto_mappings = 0
let g:jedi#popup_on_dot = 0
autocmd FileType python setlocal completeopt-=preview

"===============================================================
" Syntastic configs
"===============================================================
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 0
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_python_checkers = ['flake8']

"===============================================================
" Gitgutter Configs
"===============================================================
let g:gitgutter_max_signs = 500

"===============================================================
" python-vim configs 
"===============================================================
let python_highlight_all = 1

"===============================================================
" vim-go configs 
"===============================================================
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_fields = 1
let g:go_highlight_types = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1
let g:go_fmt_command = "goimports"
let g:go_fmt_fail_silently = 1
let g:go_fmt_autosave = 0
let g:go_play_open_browser = 0


"===============================================================
" ansible vault augroup
"===============================================================
augroup ansible-vault
    autocmd!
    autocmd BufReadPre,FileReadPre vault.yml setlocal viminfo=
    autocmd BufReadPre,FileReadPre vault.yml setlocal noswapfile noundofile nobackup
    autocmd BufReadPost,FileReadPost vault.yml silent %!ansible-vault decrypt
    autocmd BufWritePre,FileWritePre vault.yml silent %!ansible-vault encrypt
    autocmd BufWritePost,FileWritePost vault.yml silent undo
augroup END
