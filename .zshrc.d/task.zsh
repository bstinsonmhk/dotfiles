function _rnr(){
# Read aNd Review
    # TODO: This workflow would be better
    # 1.) wget a URL
    # 2.) pull the title from html
    # 3.) add a task 

    if [ "$#" -ne 1 ];
    then
        echo 'Usage: rnr <url_to_read>'
        return 1
    fi

    url=$1
    title="Read this: $(wget -O- ${url} 2>/dev/null | html2 2>/dev/null| grep '/html/head/title' | awk -F'=' '{ print $2 }')"
    
    taskid=$(task add +rnr +next ${title} 2>/dev/null | grep 'Created' | tr -d '.' | awk '{ print $NF }')
    task ${taskid} annotate \"${url}\"
}

function _rnd(){
# Research aNd Development

}

function inbox(){
    if [ "$#" -eq 0 ];
    then
        task inbox;
        return
    fi

    task add +inbox $@
}

tickle () {
    deadline=$1
    shift
    in +tickle wait:$deadline $@
}

alias bump=tickle
alias in=inbox
