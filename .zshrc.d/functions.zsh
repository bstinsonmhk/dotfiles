#!/usr/bin/zsh

function _parse_branch(){
  ref=$(git symbolic-ref HEAD 2>/dev/null) || return
  echo ${ref##refs/heads/}
}

function _prompt_char() {
  git branch >/dev/null 2>/dev/null && echo '±' && return
  echo '•'
}

function _parse_git_branch(){
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/at \1/'
}

function _inbox_tasks(){
  if [ ! -x /usr/bin/task ];
  then
      return;
  fi

  NUMTASKS=${$(task +inbox +PENDING count 2>/dev/null):-0}
  if [ $NUMTASKS -gt 0 ];
  then
      echo "(${NUMTASKS})";
  fi
}

#set the prompt so something more pretty
function setprompt () {
	autoload colors zsh/terminfo
	if [[ "$terminfo[colors]" -ge 8 ]]; then
		colors
	fi
	for COLOR in RED GREEN YELLOW BLUE MAGENTA CYAN WHITE BLACK; do
		eval PR_$COLOR='%{$fg[${(L)COLOR}]%}'
		eval PR_BRIGHT_$COLOR='%{$fg_bold[${(L)COLOR}]%}'
	done
	PR_LIGHT_BLUE="$(print '%{\e[1;36m%}')"
	PR_RESET="%{$reset_color%}"
	PR_NO_COLOR="%{$terminfo[sgr0]%}"

	case $TERM in
		rxvt*|xterm*)
			PR_TITLEBAR=$'%{\e]0;%(!.-=*[ROOT]*=- | .)%n@%m:%~ | ${COLUMNS}x${LINES} | %y\a%}'
			;;
		screen)
			PR_TITLEBAR=$'%{\e_screen \005 (\005t) | %(!.-=[ROOT]=- | .)%n@%m:%~ | ${COLUMNS}x${LINES} | %y\e\\%}'
			;;
		*)
			PR_TITLEBAR=''
			;;
	esac

    PROMPT='${PR_BLUE}%n${PR_NO_COLOR} on ${PR_MAGENTA}%m${PR_NO_COLOR} in ${PR_GREEN}%~${PR_NO_COLOR} ${PR_YELLOW}$(_parse_branch)${PR_NO_COLOR}
${PR_RED}$(_inbox_tasks)${PR_NO_COLOR}$(_prompt_char) '
    RPROMPT='%(?.. ${PR_RED}E:%?)${PR_NO_COLOR}'
	PROMPT2='${PR_LIGHT_BLUE}>${PR_NO_COLOR}'
	RPROMPT2='%_'
}

function nukehost() {
  if [[ $# -ne 1 ]]; then
      echo "USAGE: nukehost <hostname>"
      return 1
  fi

  echo "REMOVING HOST: ${1}"
  ssh-keygen -R $1 

  IP=$(host ${1} | grep "address" | awk '{ print $NF }')

  echo "REMOVING HOST IP: ${IP}"
  ssh-keygen -R $IP
}

function getpass() {
    date +%N | sha256sum | base64 | head -c 9; echo
}

function bi () {
    cd ~/src/rpmbuild && rpmbuild -bi SPECS/$1
}

function ba () {
    cd ~/src/rpmbuild && rpmbuild -ba SPECS/$1
}

function buildfromramdisk(){
    mock --rebuild --no-cleanup-after $1 >/dev/null 2>&1 && cp /var/lib/mock/epel-6-x86_64/result/*.rpm /home/bstinson/src/cpanrpms/built/ && rm $1 || echo "BUILDING: $1 failed"
}

function buildsrpm(){
    SRPM=`rpmbuild -bs $1 | awk -F':' '{ print $2 }'`
    if [[ $? -eq 0 ]]; then
        cp -v $SRPM /mnt/ramdisk/
    fi
}

function newpythonproj(){
    name=$1
    base=${HOME}/src/${name}
    mkdir -v -p ${base}/${name}
    cd $base
    touch LICENSE
    touch README.mkd
    touch setup.py
    echo '*.py[co]\n*.egg-info' >> .gitignore
    mkdir tests
    git init .
    mkvirtualenv ${name}
}
