setopt INC_APPEND_HISTORY SHARE_HISTORY
setopt APPEND_HISTORY
setopt sh_word_split
setopt prompt_subst

LD_LIBRARY_PATH=/home/bstinson/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH

MANPATH=/home/bstinson/share/man:${MANPATH}
export MANPATH

export PAGER=less
export LESS="-iMSx4 -FX"
