function rebuild_what() {
  # For all the spec files under this directory, find the BuildRequires that are
  # in EPEL. 

  for specfile in ./**/*.spec
  do
      rpmspec -P $specfile | grep Requires | awk '{ print $NF }' | xargs -I{} repoquery -q --qf="%{name} %{repoid}" --resolve {} | grep 'epel'
  done
}
