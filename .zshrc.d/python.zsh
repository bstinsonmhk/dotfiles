#Python Options
WORKON_HOME=${HOME}/.virtualenvs

export WORKON_HOME
export VIRTUALENVWRAPPER_LOG_DIR="$WORKON_HOME"
export VIRTUALENVWRAPPER_HOOK_DIR="$WORKON_HOME"
[ -e "$(which virtualenvwrapper.sh 2>/dev/null)" ] && source $(which virtualenvwrapper.sh)
