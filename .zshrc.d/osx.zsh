if [[ `uname -s` == 'Darwin' ]]
then
    # -G on MacOSX turns on colors in the shell
    alias ls='ls -G'
    PATH=/usr/local/git/bin:${PATH}
    export PATH

    # Fix pager issues when sending colors to it
    export LESS='-erX'
fi
