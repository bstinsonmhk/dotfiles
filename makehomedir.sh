#!/bin/bash - 
#===============================================================================
#
#          FILE:  makehomedir.sh
# 
#         USAGE:  ./makehomedir.sh 
# 
#   DESCRIPTION:  Run this from the downloaded git repo to setup links to the dotfiles contained in the repo 
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR: Brian Stinson (fgm), bstinson@ksu.edu
#       COMPANY: Kansas State University
#       CREATED: 11/02/2010 01:36:57 PM CDT
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

BASEDIR="/bin"
[ $(uname -o) == 'Solaris' ] && BASEDIR="/usr/gnu/bin"

TMPDIR="${HOME}/tmp/olddotfiles"
THEDATE=`date +%s`
THISDIR=$(dirname `readlink -f ${0} | sed "s:$(readlink -f ${HOME}):${HOME}:"`)
IGNOREFILES=".git .hg .hgsub .hgsubstate makehomedir.sh .submodules .hgignore"
#GITFILES=`/bin/ls -d ${THISDIR}/.[A-Za-z0-9]* | grep -v -e ".git\b" -e".hg\b" -e".hgsub\b" -e".hgsubstate\b"`
HGFILES=`/usr/bin/find ${THISDIR} -maxdepth 1 -depth $(for i in $IGNOREFILES; do echo "-not -name $i"; done) | head -n -1`

[ ! -d $TMPDIR ]; mkdir -p ${TMPDIR}

for FILE in $HGFILES
do
	FILE=$(basename ${FILE})
	HOMEFILE=${HOME}/${FILE}
	if [ -e ${HOMEFILE} -a ! -h ${HOMEFILE} ]; 
	then
		if ! ${BASEDIR}/mv -v ${HOMEFILE} ${TMPDIR}/${THEDATE}$(basename ${HOMEFILE})
		then
			exit $?
		fi
	elif [ -e ${HOMEFILE} -a -h ${HOMEFILE} ];
	then
		echo "FILE IS ALREADY LINKED: $FILE"
        continue
	else
		echo "DID NOT FIND ${HOME}/${FILE}"
	fi

	if ! ${BASEDIR}/ln -sfv ${THISDIR}/${FILE} ${HOMEFILE}
	then
		exit $?
	fi
done

# Get all vim subrepos
command -v nvim >/dev/null 2>&1 && nvim +'PlugInstall --sync' +qa ||:

# Link Tracked /bin /lib /doc /etc Files


