from ipy3status import Status

status = Status(standalone=True)

status.register("clock", format="%a %d-%b-%Y %R")
