# -*- coding: utf-8 -*-
import subprocess
import os

from libqtile.config import Key, Screen, Group, Drag, Click, Match
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook

mod = "mod1"

@hook.subscribe.startup
def dbus_register():
    x = os.environ['DESKTOP_AUTOSTART_ID']
    subprocess.Popen(['dbus-send',
                      '--session',
                      '--print-reply=string',
                      '--dest=org.gnome.SessionManager',
                      '/org/gnome/SessionManager',
                      'org.gnome.SessionManager.RegisterClient',
                      'string:qtile',
                      'string:' + x])

class Commands(object):
    browser = 'google-chrome'
    dmenu = 'dmenu_run -i -b -p ">>>"'
    terminal = 'urxvt256c-ml'
    lock_screen = 'xscreensaver-command -lock'
    raise_volume = 'amixer sset Master 2dB+'
    lower_volume = 'amixer sset Master 2dB-'
    toggle_volume = 'amixer sset Master 1+ toggle'

keys = [

    # Navigation
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "k", lazy.layout.up()),

    # Navigate between screens
    Key([mod], 'w', lazy.to_screen(0)),
    Key([mod], 'e', lazy.to_screen(1)),

    Key([mod, "shift"], "h", lazy.layout.swap_left()),
    Key([mod, "shift"], "l", lazy.layout.swap_right()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up()),

    # Window size
    Key([mod], "i", lazy.layout.grow()),
    Key([mod], "m", lazy.layout.shrink()),
    Key([mod], "n", lazy.layout.normalize()),
    Key([mod], "o", lazy.layout.maximize()),

    Key([mod, "shift"], "space", lazy.layout.flip()),


    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.nextlayout()),
    Key([mod, "shift"], "Tab", lazy.prevlayout()),

    # Commands/Actions
    Key([mod, "control"], "q", lazy.spawn('gnome-session-quit --logout --no-prompt')),
    Key([mod, "shift"], "p", lazy.spawn(Commands.dmenu)),
    Key([mod, "shift"], "Return", lazy.spawn(Commands.terminal)),
    Key([], 'XF86AudioRaiseVolume', lazy.spawn(Commands.raise_volume)),
    Key([], 'XF86AudioLowerVolume', lazy.spawn(Commands.lower_volume)),
    Key([], 'XF86AudioMute', lazy.spawn(Commands.toggle_volume)),


    Key([mod, "shift"], 'c', lazy.window.kill()),
]

groupconfigs = (

    ('1-web', { 'layout': 'MonadTall',
                'matches': [Match(wm_class=('Firefox','Google-chrome'))],
              }),
    ('2-messaging', { 'layout': 'Max',

              }),
    ('3-vim', { 'layout': 'MonadTall',
                'matches': [Match(wm_class=('gvim'))],
              }),
    ('4-media', { 'layout': 'Max',
                }),
    ('4-misc',  { 'layout': 'Max',
                }),
)

groups = [Group(i) for i in ['1-web','2-messaging','3-vim','4-media','5-misc'] +
                            map(str, range(6,10))
         ]

for idx,i in enumerate(groups):
    # mod1 + letter of group = switch to group
    keys.append(
        Key([mod], str(idx+1), lazy.group[i.name].toscreen())
    )

    # mod1 + shift + letter of group = switch to & move focused window to group
    keys.append(
        Key([mod, "shift"], str(idx+1), lazy.window.togroup(i.name))
    )

layouts = [
    layout.MonadTall(),
    layout.Matrix(columns=2, name='2up'),
    layout.Max(),
]

widget_defaults = dict(
    fontsize=16,
    padding=5,
    background='#282828',
)

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Volume(theme_path='/usr/share/icons/gnome/22x22/status'),
                widget.Systray(),
                widget.GroupBox(),
                widget.Sep(),
                widget.YahooWeather(location='Manhattan, KS',
                                    format='{condition_text} {condition_temp}°',
                                    metric=False),
                widget.Sep(),
                widget.TextBox("MHK: "),
                widget.Clock(format='%Y-%m-%d %a %H:%M'),
                widget.TextBox("UTC: "),
                widget.Clock(timezone='UTC', format='%Y-%m-%d %a %H:%M'),
                widget.Sep(),
                widget.Notify(),
                widget.Spacer(),
                widget.Sep(),
                widget.TextBox("bstinson config", name="bstinson"),
                widget.Sep(),
                widget.CurrentLayout(),
            ],
            30,
            background='#282828',
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
        start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
        start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating()
auto_fullscreen = True

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, github issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
