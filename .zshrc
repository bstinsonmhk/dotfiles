export HISTFILE=~/.histfile
export HISTORY=20000
export SAVEHIST=20000
export HISTIGNORE="&:ls:[bf]g:exit:reset:clear:cd:cd ..:cd.."
export MANPAGER="less"
export EDITOR="vim"
export LC_ALL="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
export LC_TIME="en_US.UTF-8"
zmodload zsh/terminfo

function load_zsh_configs(){
  for filename in "${1}/"*.zsh
  do
      if [[ -f ${filename} ]]; 
      then
          source ${filename}
      fi
  done
}

if [[ -x /usr/bin/dircolors || -x /bin/dircolors ]]; then
	eval "`dircolors -b ~/.dircolors`"
	alias ls='ls --color=auto'
	alias grep='grep --color=auto'
	alias fgrep='fgrep --color=auto'
	alias egrep='egrep --color=auto'
fi
#Load definitions from other files
load_zsh_configs "${HOME}/.zshrc.d"

setprompt

#The following lines were added by compinstall

# Enable completion caching, use rehash to clear
zstyle ':completion::complete:*' use-cache on
zstyle ':completion::complete:*' cache-path ~/.cache/zsh

zstyle ':completion:*' completer _expand _complete _ignored _approximate 
zstyle ':completion:*:match' original only
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:kill:*' force-list always
zstyle ':completion:*' group-name ''
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' matcher-list '+m:{a-z}={A-Z} r:|[._-]=** r:|=**' '' '' '+m:{a-z}={A-Z} r:|[._-]=** r:|=**'
zstyle ':completion:*' max-errors 1 numeric
zstyle :compinstall filename '/home/bstinson/.zshrc'

source ${HOME}/.zshcomps

bindkey -v
bindkey '\e[3~' delete-char
bindkey '^R' history-incremental-search-backward

autoload -Uz compinit
compinit
# End of lines added by compinstall


PERL5LIB=${HOME}/lib

PATH=/usr/local/bin:${HOME}/bin:${HOME}/sbin:${PATH}:/opt/java/bin:${HOME}/.local/bin

if [[ -e /usr/texbin ]];
then
    PATH=${PATH}:/usr/texbin
fi

export PATH
#Aliases
alias vless='vim -u /usr/share/vim/vim73/macros/less.vim'

export LESS="-iR"
