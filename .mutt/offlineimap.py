import subprocess

def getpasswd(passwd):

   cmd = subprocess.Popen(['/usr/bin/pass', passwd],stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE)

   (out,err) = cmd.communicate()

   if cmd.returncode == 0:
       return out.strip()

